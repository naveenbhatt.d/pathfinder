#include<stdio.h>
#define infinity 3200
#define dim 6
#define k 100
#define l 1024
#define m 9009

extern int maxVertices;
extern int processed_matrix[dim][dim];
extern int nodeLocation[dim][2];

/*************************Testing module for processed matrix*******

void print(){
    for(int i=0;i<dim;i++){
        for(int j=0;j<dim;j++){
         printf(" %d ",processed_matrix[i][j]);
        }
        printf("\n");
    }
}
*************************************************************************/
/************************************************************************
CountNodes: This function calculates the total number of nodes in a graph
with less than v vertices.
**************************************************************************
**************************************************************************/

int CountNodes(){
    int count=0;
    for(int i=0;i<dim;i++){
        for(int j=0;j<dim;j++){
            if(processed_matrix[i][j]<=maxVertices)
             {
               nodeLocation[count][0]=i;
               nodeLocation[count][1]=j;
               count++;
              // printf("i=%d, j=%d \n",i,j);
              }
          }
     }
return count;
}
/*************************************************************************
************************************************************************
WeightMatrix: This function transforms the given processed matrix into a
weighted adjacency matrix.
**************************************************************************
**************************************************************************/


void weightMatrix()
{
    int nodes=CountNodes();
    int weightMatrix[nodes][nodes];
    int l1=0;
    int l2=0;
    int edge=0;
    int edgeLength=0;

    for(int i=0;i<nodes;i++){
        for(int j=0;j<nodes;j++){
        if(i!=j)
        weightMatrix[i][j]=infinity;
        else
        weightMatrix[i][j]=0;
        }
    }


    for(int i=0;i<nodes;i++){
        int index=0;
        l1=nodeLocation[i][0];
        l2=nodeLocation[i][1];
        //printf(" l1=%d l2=%d \n",l1,l2);
        //Probe right and find the edge weight to update wieght matrix
        if((l2+1)<dim && processed_matrix[l1][l2+1]==l){
          edgeLength=findPathRight(l1,l2+1,1);
          //printf("edge length for node (%d,%d) = %d \n",i,edge,edgeLength);
          if(edgeLength>1){
            index=getNodeRight(l1,l2+1);

            weightMatrix[i][index]=edgeLength;
            weightMatrix[index][i]=edgeLength;
            }
        }

          //Probe Down and find the edge weight to update wieght matrix
        if((l1+1)<dim && processed_matrix[l1+1][l2]==l){
          edgeLength=findPathDown(l1+1,l2,1);
          if(edgeLength>1){
             index=getNodeDown(l1+1,l2);
            weightMatrix[i][index]=edgeLength;
            weightMatrix[index][i]=edgeLength;
            }
        }
          //Probe diag and find the edge weight to update wieght matrix





    }
    // ***********Print the content of weight matrix*************

  printf("\n Content of Weight matrix \n");
    for(int i=0;i<nodes;i++){
        for(int j=0;j<nodes;j++){
        printf("  %d  ",weightMatrix[i][j]);
        }
        printf("\n");
    }
printf("Calling shortest path algorithm");
path(nodes,weightMatrix);

}


int findPathRight(int i,int j,int count){
    if(    processed_matrix[i][j]==0
        || processed_matrix[i][j]==1
        || processed_matrix[i][j]==2
        || processed_matrix[i][j]==3
        || processed_matrix[i][j]==4 )
        return 0;
     //Check  right for possible path
    if((j+1)<dim && processed_matrix[i][j]==l){

    processed_matrix[i][j]=processed_matrix[i][j]+k;
    return findPathRight(i,j+1,count)+count;
    }
   else return 0;
}
//Node value for right probe
int getNodeRight(int i,int j){
     if(    processed_matrix[i][j]==0
        || processed_matrix[i][j]==1
        || processed_matrix[i][j]==2
        || processed_matrix[i][j]==3
        || processed_matrix[i][j]==4 )
        return processed_matrix[i][j];
    else if((j+1)<dim && processed_matrix[i][j]==l+k){
     return getNodeRight(i,j+1);
    }

}

int findPathDown(int i,int j,int count){
    if(    processed_matrix[i][j]==0
        || processed_matrix[i][j]==1
        || processed_matrix[i][j]==2
        || processed_matrix[i][j]==3
        || processed_matrix[i][j]==4 )
        return 0;

    if((i+1)<dim && processed_matrix[i][j]==l){

    processed_matrix[i][j]=processed_matrix[i][j]+k;
    return findPathDown(i+1,j,count)+count;
}
   else return 0;
}
//Node value for right probe
int getNodeDown(int i,int j){
     if(   processed_matrix[i][j]==0
        || processed_matrix[i][j]==1
        || processed_matrix[i][j]==2
        || processed_matrix[i][j]==3
        || processed_matrix[i][j]==4 )
        return processed_matrix[i][j];
    else if((i+1)<dim && processed_matrix[i][j]==l+k){
    return getNodeDown(i+1,j);
    }

}
