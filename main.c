#include <stdio.h>
#include <stdlib.h>
#define infinity 32000
#include "test.c"
#include "path.c"
#include "weightMatrix.c"

#define dim 6
#define k 100
#define l 1024
#define m 9009
/**********************************************************************************
Processed matrix (2-D array) is an sample array of pixel intensity values,
that contains vertices as some distinct unique numbers.
Edges between any two vertices are represented as constant intensity (l).
If there doesn't exists a path then pixel intensity for all such pixels are marked as m.
this processed matrix has to be replaced with acutal process matrix from
an image but the logic to extract information of edges and vertices remains same.
*************************************************************************************/

int processed_matrix[dim][dim]={{0,l,l,l,l,1},
                                {l,m,m,m,m,l},
                                {l,m,m,m,m,l},
                                {l,m,m,m,m,l},
                                {l,m,m,m,m,l},
                                {2,l,l,l,l,3}};
int nodeLocation[dim][2];

int maxVertices=10;

int main(){

    printf("Project- Path Finder\n\n");

    printf("\n");
    weightMatrix();


/******************Testing Module******************
    print();
    path(dimension,inputMatrix);
    test();
**************************************************/
    return 0;
}
